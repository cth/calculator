package util

import model.expression.BinaryOperator

fun Char.isBinaryOperator() = BinaryOperator.values().map { it.symbol }.contains("$this")

val unaryOperators = setOf('+', '-')
fun Char.isUnaryOperator() = unaryOperators.contains(this)

inline fun <T, R : Comparable<R>> Iterable<T>.listOfMinBy(selector: (T) -> R): List<T> {
    val minValue = minBy(selector)?.let { selector(it) } ?: return emptyList()
    return this.filter { selector(it) == minValue }
}