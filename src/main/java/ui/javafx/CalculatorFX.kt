package ui.javafx

import javafx.application.Application
import javafx.stage.Stage
import tornadofx.*
import kotlin.reflect.KClass

class CalculatorFX : App()
{
    override val primaryView = CalculatorWindow::class
}
