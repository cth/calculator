import javafx.application.Application
import model.expression.ExpressionParser
import ui.javafx.CalculatorFX

fun main(args: Array<String>)
{
    Application.launch(CalculatorFX::class.java, *args)
}