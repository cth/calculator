package model.expression.Nodes

import java.math.BigDecimal

data class ConstantNode(val value: BigDecimal) : Node
{
    override fun evaluate() = value
}