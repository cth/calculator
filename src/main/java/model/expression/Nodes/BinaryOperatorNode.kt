package model.expression.Nodes

import model.expression.BinaryOperator

data class BinaryOperatorNode(val operator: BinaryOperator,
                              val first: Node,
                              val second: Node) : Node
{
    override fun evaluate() = operator.operate(first.evaluate(), second.evaluate())
}

