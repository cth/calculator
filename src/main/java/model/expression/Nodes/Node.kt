package model.expression.Nodes

import java.math.BigDecimal

interface Node {
    fun evaluate(): BigDecimal
}