package model.expression

import model.expression.Nodes.Node
import model.expression.Tokens.*
import util.*
import java.math.BigDecimal

class ExpressionParser(val expression: String)
{
    /**
     * [expression] string with whitespace removed.
     */
    val pruned = expression.filterNot(Char::isWhitespace)

    val tokens: List<Token<*, *>> = run {

        var state: State = State.START
        var acc = ""
        val tokens = mutableListOf<Token<*, *>>()

        for (c in pruned)
        {
            try
            {
                val pair = state.parseNextChar(c, acc, tokens)
                acc = pair.first
                state = pair.second
            }
            catch (e: Exception)
            {
                throw Exception("Exception in String \"$pruned\"", e)
            }
        }

        if (acc.isNotBlank())
        {
            state.completeToken(acc, tokens)
        }

        tokens
    }

    val rootNode: Node = run {

        fun parse(listOfTokens: List<Token<*, *>>): Node
        {
            val leastSignificantAll = listOfTokens.filterIsInstance<BinaryOperatorToken>().listOfMinBy { it.operator }

            if (leastSignificantAll.isEmpty())
            {
                // No binary operators left, so there *should* only be a single token, a constant.
                // TODO: Figure out how to handle cases where the above is not true
                if (listOfTokens.size == 1)
                {
                    return (listOfTokens[0] as? ConstantToken)?.toNode(Unit)
                           ?: (listOfTokens[0] as? ParenthesesExpressionToken)?.toNode(Unit)
                           ?: throw Exception("Unexpected token: ${listOfTokens[0]}")
                }
                else
                {
                    throw Exception("Expected one token, received ${listOfTokens.size}: $listOfTokens")
                }
            }
            else
            {
                val leastSignificantRightmost = leastSignificantAll.last()
                val indexLeastSignificant = listOfTokens.indexOf(leastSignificantRightmost)

                return leastSignificantRightmost.toNode(BinaryOperatorToken.ExtraInfo(
                        parse(listOfTokens.slice(0..indexLeastSignificant - 1)),
                        parse(listOfTokens.slice(indexLeastSignificant + 1 until listOfTokens.size))))
            }
        }

        parse(tokens)
    }


    /**
     * String parsing state machine.
     *
     * Parameters are "Current character", "token so far", and "list of tokens so far"
     * Return values mean "Token so far" and "Next state"
     */
    private enum class State(val parseNextChar: (Char, String, MutableList<Token<*, *>>) -> Pair<String, State>,
                             val completeToken: (String, MutableList<Token<*, *>>) -> Unit)
    {
        START(
                { c, _, _ ->
                    when
                    {
                        c.isUnaryOperator() -> Pair("$c", LEFT_OF_DECIMAL)

                        c.isDigit() -> Pair("$c", LEFT_OF_DECIMAL)

                        c == '.' -> Pair("$c", RIGHT_OF_DECIMAL)

                        c == '(' ->
                        {
                            READING_PARENTHESES.parenthesisStackCount = 1
                            Pair("", READING_PARENTHESES)
                        }

                        else -> throw Exception("Unexpected character '$c'")
                    }
                },
                { _, _ -> Unit }),

        LEFT_OF_DECIMAL(
                { c, acc, tokens ->
                    when
                    {
                        c.isDigit() -> Pair(acc + c, LEFT_OF_DECIMAL)

                        c == '.' -> Pair(acc + c, RIGHT_OF_DECIMAL)

                        c.isBinaryOperator() ->
                        {
                            LEFT_OF_DECIMAL.completeToken(acc, tokens)
                            Pair("$c", READING_BINARY_OPERATOR)
                        }

                        c == '(' ->
                        {
                            LEFT_OF_DECIMAL.completeToken(acc, tokens)
                            // Add implicit multiplication
                            READING_BINARY_OPERATOR.completeToken("*", tokens)
                            READING_PARENTHESES.parenthesisStackCount = 1
                            Pair("", READING_PARENTHESES)
                        }

                        else -> throw Exception("Unexpected character '$c'")
                    }
                },
                { acc, tokens -> tokens.add(ConstantToken(BigDecimal(acc))) }
        ),

        RIGHT_OF_DECIMAL(
                { c, acc, tokens ->
                    when
                    {
                        c.isDigit() -> Pair(acc + c, RIGHT_OF_DECIMAL)

                        c.isBinaryOperator() ->
                        {
                            RIGHT_OF_DECIMAL.completeToken(acc.trim('.'), tokens)
                            Pair("", READING_BINARY_OPERATOR)
                        }

                        c == '(' ->
                        {
                            RIGHT_OF_DECIMAL.completeToken(acc.trim('.'), tokens)
                            // Add implicit multiplication
                            READING_BINARY_OPERATOR.completeToken("*", tokens)
                            READING_PARENTHESES.parenthesisStackCount = 1
                            Pair("", READING_PARENTHESES)
                        }

                        else -> throw Exception("Unexpected character '$c'")
                    }
                },
                { acc, tokens -> tokens.add(ConstantToken(BigDecimal(acc))) }
        ),

        READING_BINARY_OPERATOR(
                { c, acc, tokens ->
                    when
                    {
                        c.isBinaryOperator() -> Pair("$c", READING_BINARY_OPERATOR)

                        c.isDigit() ->
                        {
                            READING_BINARY_OPERATOR.completeToken(acc, tokens)
                            Pair("$c", LEFT_OF_DECIMAL)
                        }

                        c.isUnaryOperator() ->
                        {
                            READING_BINARY_OPERATOR.completeToken(acc, tokens)
                            Pair("$c", LEFT_OF_DECIMAL)
                        }

                        c == '.' ->
                        {
                            READING_BINARY_OPERATOR.completeToken(acc, tokens)
                            Pair("$c", RIGHT_OF_DECIMAL)
                        }

                        c == '(' ->
                        {
                            READING_BINARY_OPERATOR.completeToken(acc, tokens)
                            READING_PARENTHESES.parenthesisStackCount = 1
                            Pair("", READING_PARENTHESES)
                        }

                        else -> throw Exception("Unexpected character '$c'")
                    }
                },
                { acc, tokens -> tokens.add(BinaryOperatorToken(BinaryOperator.get(acc))) }
        ),

        READING_PARENTHESES(
                { c, acc, tokens ->
                    when
                    {
                        c == '(' && READING_PARENTHESES.parenthesisStackCount > 0 ->
                        {
                            READING_PARENTHESES.parenthesisStackCount++
                            Pair(acc + c, READING_PARENTHESES)
                        }

                        c == ')' ->
                        {
                            READING_PARENTHESES.parenthesisStackCount--
                            if (READING_PARENTHESES.parenthesisStackCount == 0)
                            {
                                READING_PARENTHESES.completeToken(acc, tokens)
                                // Since we don't know what comes after the parentheses,
                                // stay in this state until we do know and handle that case in the
                                // "else" branch of the "when" block
                                Pair("", READING_PARENTHESES)
                            }
                            else
                            {
                                Pair(acc + c, READING_PARENTHESES)
                            }
                        }

                        else ->
                            if (READING_PARENTHESES.parenthesisStackCount == 0)
                            {
                                when
                                {
                                    c.isDigit() ->
                                    {
                                        // Add implicit multiplication
                                        READING_BINARY_OPERATOR.completeToken("*", tokens)
                                        Pair("$c", LEFT_OF_DECIMAL)
                                    }

                                    c == '.' ->
                                    {
                                        // Add implicit multiplication
                                        READING_BINARY_OPERATOR.completeToken("*", tokens)
                                        Pair("$c", RIGHT_OF_DECIMAL)
                                    }

                                    c == '(' ->
                                    {
                                        READING_PARENTHESES.parenthesisStackCount = 1
                                        // Add implicit multiplication
                                        READING_BINARY_OPERATOR.completeToken("*", tokens)
                                        Pair("", READING_PARENTHESES)
                                    }

                                    c.isBinaryOperator() ->
                                    {
                                        Pair("$c", READING_BINARY_OPERATOR)
                                    }

                                    else -> throw Exception("Unexpected character '$c'")
                                }
                            }
                            else
                            {
                                Pair(acc + c, READING_PARENTHESES)
                            }
                    }
                },
                { acc, tokens -> tokens.add(ParenthesesExpressionToken(acc)) }
        );

        /**
         * Only used for [READING_PARENTHESES] state
         */
        private var parenthesisStackCount = 0
    }


}