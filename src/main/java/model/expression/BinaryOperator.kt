package model.expression

import java.math.BigDecimal

enum class BinaryOperator(val symbol: String,
                          val operate: (BigDecimal, BigDecimal) -> BigDecimal,
                          private val order: Comparable<*>) : Comparable<BinaryOperator>
{
    // TODO: Support for multiple symbols (Do one symbol for printing and a set of all valid symbols for reading)
    PLUS("+", { a, b -> a + b }, 1),
    MINUS("-", { a, b -> a - b }, 1),
    TIMES("*", { a, b -> a * b }, 2),
    DIVIDE("/", { a, b -> a / b }, 2),
    // TODO: Use a proper BigDecimal exponent operation
    POWER("^", { a, b -> BigDecimal(Math.pow(a.toDouble(), b.toDouble())) }, 3);

    companion object {
        fun get(c: String) = BinaryOperator.values().find { it.symbol == c } ?:
                             throw Exception("No such binary operator: $c")
    }
}