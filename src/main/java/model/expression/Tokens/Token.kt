package model.expression.Tokens

import model.expression.Nodes.Node

/**
 * T: Data class representing extra information needed to create the node
 */
interface Token<out U: Node, in T>
{
    fun toNode(extraInfo: T): U
}