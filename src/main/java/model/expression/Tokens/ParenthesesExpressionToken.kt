package model.expression.Tokens

import model.expression.ExpressionParser
import model.expression.Nodes.Node

class ParenthesesExpressionToken(val expression: String) : Token<Node, Unit>
{
    override fun toNode(extraInfo: Unit) = ExpressionParser(expression).rootNode
}