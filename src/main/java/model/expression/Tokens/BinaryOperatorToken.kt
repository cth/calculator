package model.expression.Tokens

import model.expression.BinaryOperator
import model.expression.Nodes.*

class BinaryOperatorToken(val operator: BinaryOperator) : Token<BinaryOperatorNode, BinaryOperatorToken.ExtraInfo> {

    override fun toNode(extraInfo: ExtraInfo) = BinaryOperatorNode(operator, extraInfo.first, extraInfo.second)

    data class ExtraInfo(val first: Node, val second: Node)
}