package model.expression.Tokens

import model.expression.Nodes.ConstantNode
import java.math.BigDecimal

class ConstantToken(val value: BigDecimal) : Token<ConstantNode, Unit>
{
    override fun toNode(extraInfo: Unit) = ConstantNode(value)
}