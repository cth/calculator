This project aims to provide the following:

- Evaluation for arithmetic expressions
- Evaluation of algebraic systems of equations
- Evaluation of derivatives and integrals
- Support for vectors and matrix algebra

- Simple, intuitive syntax
- Headless mode to make automation easy
- Plugin support